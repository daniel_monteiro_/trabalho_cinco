# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import datetime


def generate_graphic_pdf(list_acertos):
    plt.plot(list_acertos)
    plt.ylabel("Taxas de acertos")
    plt.xlabel("Rodada de treinamento")

    plt.savefig('graphics/acertos_' + datetime.datetime.now().strftime("%d_%m_%Y__%H_%M_%S") + '.pdf')